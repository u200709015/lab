 public class FindPrimes {
    public static void main(String[] args) {
        int value = Integer.parseInt(args[0]);

        if (value<2){
            System.out.println("It's not greater than 1.");
        }
        for (int i=2;i<value;i++){
            int divisior =2;
            boolean isPrime = true;

            while(divisior<1 && isPrime){
                if(i%divisior==0){
                    isPrime=false;
                divisior++;
                }
                if(isPrime){
                    System.out.println(i+" ");
                }
            }
        }
    }
}